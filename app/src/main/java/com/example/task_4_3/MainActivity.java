package com.example.task_4_3;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import utils.Calculator;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.first_number)
    EditText first_number;

    @BindView(R.id.second_number)
    EditText second_number;

    @BindView(R.id.third_number)
    EditText third_number;

    @BindView(R.id.first_number_view)
    TextView first_number_view;

    @BindView(R.id.second_number_view)
    TextView second_number_view;

    @BindView(R.id.third_number_view)
    TextView third_number_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        first_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int firstNumberInt = Integer.parseInt(editable.toString().trim());

                String firstNumber = Calculator.getBinaryNumber(firstNumberInt);
                first_number_view.setText(firstNumber);

                String secondNumber = Calculator.getOctalNumber(firstNumberInt);
                second_number_view.setText(secondNumber);

                String thirdNumber = Calculator.getHexaDecimalNumber(firstNumberInt);
                third_number_view.setText(thirdNumber);
            }
        });

        second_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int secondNumberInt = Integer.parseInt(editable.toString().trim());

                String firstNumber = Calculator.getBinaryNumber(secondNumberInt);
                first_number_view.setText(firstNumber);

                String secondNumber = Calculator.getOctalNumber(secondNumberInt);
                second_number_view.setText(secondNumber);

                String thirdNumber = Calculator.getHexaDecimalNumber(secondNumberInt);
                third_number_view.setText(thirdNumber);
            }
        });

        third_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int thirdNumberInt = Integer.parseInt(editable.toString().trim());

                String firstNumber = Calculator.getBinaryNumber(thirdNumberInt);
                first_number_view.setText(firstNumber);

                String secondNumber = Calculator.getOctalNumber(thirdNumberInt);
                second_number_view.setText(secondNumber);

                String thirdNumber = Calculator.getHexaDecimalNumber(thirdNumberInt);
                third_number_view.setText(thirdNumber);
            }
        });
    }
}