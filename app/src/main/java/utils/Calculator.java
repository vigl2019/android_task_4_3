package utils;

public class Calculator {

    public Calculator(){}

    //================================================================================//

    public static String getBinaryNumber(int numberInt){
        return Integer.toBinaryString(numberInt);
    }

    public static String getOctalNumber(int numberInt){
        return Integer.toOctalString(numberInt);
    }

    public static String getHexaDecimalNumber(int numberInt){
        return Integer.toHexString(numberInt);
    }
}